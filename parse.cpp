#include "parse.hpp"

using namespace Eigen;
void parserLine(char* line,std::vector<std::string>& v){
	char *p = strtok(line ,",");
	if(p){
		//p = strtok(NULL,",");
	}
	while(p){
		v.push_back(p);
		p = strtok(NULL,",");
	}
}

std::vector<Universe> paserUniverse(char* filename){
	std::vector<Universe> universe;
	FILE * fp = NULL;
	char * line = NULL;
	size_t len = 0;
	ssize_t readsz;
	fp = fopen(filename, "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);
		
	while ((readsz = getline(&line, &len, fp)) != -1){
		std::vector<std::string> t;
		parserLine(line,t);
		if(t.size() != 3){
			printf("error universe");
			exit(EXIT_FAILURE);
		}
		Universe Universe;
		std::string name = t[0];
		if(name == ""){
			printf("error universe");
			exit(EXIT_FAILURE);
		}
		double avr = atof(t[1].c_str());
		if(avr==0.0  ){
			printf("error universe");
			exit(EXIT_FAILURE);
		}
		double sigma = atof(t[2].c_str());
		Universe.name = name;
		if( sigma == 0.0){
			printf("error universe");
			exit(EXIT_FAILURE);
		}
		Universe.avr = avr;
		Universe.sigma = sigma;
	
		universe.push_back(Universe);
	}
	if(universe.size() == 0){
		printf("universe file is empty");
		exit(EXIT_FAILURE);
	}
	if (line)
		free(line);
	return universe;
}

MatrixXd parserCorrelation(char* filename, std::vector<Universe> universe){
	
	FILE * fp = NULL;
	char * line = NULL;
	size_t i=0;
	size_t len = 0;
	ssize_t read;
	fp = fopen(filename, "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);
	MatrixXd corrMatrix(universe.size(), universe.size());


	
	while ((read = getline(&line, &len, fp)) != -1){
		if(i >= universe.size()){
			break;
		}
		std::vector<std::string> t;
		parserLine(line,t);
		if(t.size() != universe.size()){
			printf("error correlation");
			exit(EXIT_FAILURE);
		}
		for(unsigned int j=0;j<universe.size();j++){
			double v = atof(t[j].c_str());
			if(v == 0.0){
				printf("error correlation");
				exit(EXIT_FAILURE);
			}
			corrMatrix(i,j) = v;
		}
		i++;
	}
	if(i == 0){
		printf("correlation file is empty");
		exit(EXIT_FAILURE);
	}
	
	if (line)
		free(line);
	return corrMatrix;
}

double volatility(Portfolio &portfolio,double step,bool r){
	int num = portfolio.universe.size(); 
	MatrixXd zero2d = MatrixXd::Zero(2,2);
	MatrixXd universe(2,num);
	for(int i=0;i<num;i++){
		universe(0,i) = 1;
	}
	for(int i=0;i<num;i++){
		universe(1,i) = portfolio.universe[i].avr;
	}
	 
	MatrixXd B(num+2,1);
	for(int i=0;i<num;i++){
		B(i,0) = 0;
	}

	B(num,0) = 1;
	B(num+1,0) = step;

	MatrixXd cov(num,num);
	MatrixXd tmpCov(num+2,num+2);
	for(int i=0;i<cov.rows();i++){
		for(int j=0;j<cov.cols();j++){
			cov(i,j) = portfolio.universe[i].sigma  * portfolio.universe[j].sigma * portfolio.corrMatrix(i,j);
	 
		}
	}
	MatrixXd ta = universe.transpose();
 
	
	//printf("%ld-%ld\num",tmpCov.rows(),tmpCov.cols());
	tmpCov<<cov,ta,universe,zero2d;
	//printf("%ld-%ld\num",tmpCov.rows(),tmpCov.cols());
	VectorXd solve(num+2,1);
	solve = tmpCov.fullPivHouseholderQr().solve(B);
	if(r){
	 
		VectorXd vs = solve;

		while(1){
			bool loop_end = true;
			MatrixXd conM;
			int k = 0;
			for(int j=0; j < num; j++){
				if(vs(j) >= 0){
					continue;
				}				
				MatrixXd nzero = MatrixXd::Zero(num,1);
				nzero(j,0) = 1;
				MatrixXd temp = conM;
				conM.resize(num,k+1);
				if(k++ != 0){
					conM<<temp,nzero;
				}
				else{
					conM << nzero;
				}
				loop_end = false; 
				
			}
			if(loop_end){
				break;
			}
			MatrixXd taM = universe;
			int newARow = taM.rows() + conM.cols();
			universe.resize(newARow, num);
			universe<<taM,conM.transpose();
			MatrixXd tbM = B;
			int newBRow = tbM.rows() + conM.cols();
			B.resize(newBRow,1);
			
			B<<tbM,MatrixXd::Zero(conM.cols(),1);
			MatrixXd zeroM = MatrixXd::Zero(universe.rows(),universe.rows());
			int newRows = cov.rows() + universe.rows();
			MatrixXd tmpCov(newRows, newRows);
			tmpCov<<cov,universe.transpose(),universe,zeroM;
			vs = tmpCov.fullPivHouseholderQr().solve(B);
		}
		portfolio.w = vs.head(num);
	}else{
		portfolio.w = solve.head(num);
	}
	double sum = 0;
	for(int i=0;i<num;i++){
		for(int j=0;j<num;j++){
			double t = portfolio.w(i)*portfolio.w(j);
			sum += t * portfolio.universe[i].sigma* portfolio.corrMatrix(i,j)*portfolio.universe[j].sigma;
		}
	} 

	return (double)std::sqrt(sum);
}
