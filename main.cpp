#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <iomanip>
#include "parse.hpp"


using namespace Eigen;
int main(int argc, char ** argv){
        bool rflag = false;
        Portfolio portfolio;
        if(argc <3){
                std::cerr<<"./efficient_frontier -r universe.csv correlation.csv" <<std::endl;
                exit(1);
        }
        if(strcmp(argv[1],"-r") == 0){
                rflag = true;
                portfolio.universe = paserUniverse(argv[2]);
                portfolio.corrMatrix = parserCorrelation(argv[3],portfolio.universe  );
        }else{
                portfolio.universe = paserUniverse(argv[1]);
                portfolio.corrMatrix = parserCorrelation(argv[2],portfolio.universe );
        }

        std::cout<<"ROR,volatility"<<std::endl;
        std::cout.setf(std::ios::fixed);
        for(int i = 10;i <= 265;i += 10){
                std::cout<<std::fixed<<std::setprecision(1)<<i/10.0f<<"%,";
                double step = (i/1000.0f);
                std::cout<<std::fixed<<std::setprecision(2)<<volatility(portfolio,step,rflag)*100<<"%"<<std::endl;
        }

        return 0;
}
