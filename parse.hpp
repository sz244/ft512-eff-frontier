#ifndef __PARSE_H__
#define __PARSE_H__


#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <Eigen/Dense>
using namespace Eigen;

class Universe {
  public:
    std::string name;
    double avr;
        double w;
    double sigma;
  public:
    Universe(){}
    ~Universe(){}
};

class Portfolio {
  public:

    VectorXd w;
    std::vector<Universe> universe;
    MatrixXd corrMatrix;
  public:
    Portfolio(){}


    ~Portfolio(){}
};

std::vector<Universe> paserUniverse(char* filename);
MatrixXd parserCorrelation(char* filename, std::vector<Universe> universe);

double volatility(Portfolio &portfolio,double  step,bool r);
#endif
