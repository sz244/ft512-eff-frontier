all:efficient_frontier
efficient_frontier: main.cpp parse.cpp parse.hpp
	g++ -std=gnu++98 -Wall -Werror main.cpp parse.cpp  -o efficient_frontier

clean:
	rm -f *.o  *~ efficient_frontier
